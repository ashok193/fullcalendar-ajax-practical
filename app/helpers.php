<?php

function getMonthListFromDate($start_date,$end_date){
    $start_date = strtotime($start_date);
    $start_date = @strtotime(@date('Y-m-01', $start_date) . "-1 month");
    $end_date   = strtotime($end_date);
    $current    = $start_date;
    $ret        = array();

    while( $current<$end_date ){
        $next    = @date('Y-m-01', $current) . "+1 month";
        $current = @strtotime($next);
        $ret[]   = date('Y-m',$current);
    }

    return array_reverse($ret);
}
function getWeekListFromDate($start_date,$end_date,$event_date){
    $startDate = new DateTime($start_date);
    $endDate   = new DateTime($end_date);
    $eventDate = (new DateTime($event_date))->format('w');

    $date_week = array();

    while ($startDate <= $endDate) {
        if ($startDate->format('w') == $eventDate) {
            $date_week[] = $startDate->format('Y-m-d');
        }
        
        $startDate->modify('+1 day');
    }
    return $date_week;
}
function getDayListFromDate($start_date,$end_date){
    $days = array();
    
    $period = new DatePeriod(
        new DateTime($start_date),
        new DateInterval('P1D'),
        new DateTime($end_date.' 23:59:59')
   );

   foreach ($period as $dt) {
        $days[] = $dt->format("Y-m-d");
    }
    
    return $days;
}
function getYearListFromDate($start_date,$end_date){
    $years = array();
    
    $period = new DatePeriod(
        new DateTime($start_date),
        new DateInterval('P1Y'),
        new DateTime($end_date.' 23:59:59')
   );

   foreach ($period as $dt) {
        $years[] = $dt->format("Y");
    }
    
    return $years;
}
function validateDate($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}
?>