# Fullcalendar with Ajax Practical

Server Requirment

- PHP >= 8.0
- Mysql
- Composer
- gitbash
- node
- npm
- Also follow below link : https://laravel.com/docs/9.x/deployment

## Features

- Register User
- Login User
- Add Event
- View Fullcalendar all event with ajax filter
- Delete,Edit & view Event

## Tech

- Laravel with vite 
- boostrap 5
- sweetalert2
- fullcalendar
- [node.js] - run laravel front vite

## Installation

Dillinger requires [Node.js](https://nodejs.org/) v16.17.0+ to run.
Dillinger requires [Composer](https://getcomposer.org/Composer-Setup.exe) to run.

Install the dependencies and devDependencies and start the server.

```sh
git clone https://gitlab.com/ashok193/fullcalendar-ajax-practical.git
Rename .env.example to .env and set database configuration.
cd fullcalendar-ajax-practical
composer install
npm install
php artisan migrate
php artisan key:generate
php artisan serve
npm run dev
```
Open your favorite Terminal and run these commands.

First Tab:

```sh
php artisan serve
```

Second Tab:

```sh
npm run dev
```

Project url

```sh
http://localhost:8000/
```