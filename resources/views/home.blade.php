@extends('layouts.app')

@section('content')
@vite(['resources/js/event.js'])
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ __('Manage Event') }}
                    
                </div>

                <div class="card-body">
                    <div class="d-flex justify-content-end p-3">
                        <button type="button" class="btn btn-primary" id="add_event">Add Event</button>
                        <br>
                    </div>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <div id='calendar'></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="eventModalCenter" tabindex="-1" role="dialog" aria-labelledby="eventModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="eventModalLongTitle">Add Event</h5>
                
            </div>
            <form name="formdata" id="formdata">
                @csrf
                <input type="hidden" name="event_id" id="event_id"  value=""/>
                <input type="hidden" name="action" id="action"  value="add"/>
                <div class="modal-body">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <label for="name">{{ __('Event Name') }}</label>
                            <input id="name" type="text" class="form-control" name="name">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <label for="description">{{ __('Event Description') }}</label>
                            <textarea id="description" name="description" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <label for="start_date">{{ __('Start Date') }}</label>
                            <input id="start_date" type="date" class="form-control" name="start_date">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <label for="end_date">{{ __('End Date') }}</label>
                            <input id="end_date" type="date" class="form-control" name="end_date">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <label for="recurrence_type">{{ __('Recurrence type') }}</label>
                            <select name="recurrence_type" id="recurrence_type" class="form-control">
                                <option value="Single">Single</option>
                                <option value="Daily">Daily</option>
                                <option value="Weekly">Weekly</option>
                                <option value="Monthly">Monthly</option>
                                <option value="Yearly">Yearly</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close_modal" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('scripts')
<script type="module">
    jQuery.validator.addMethod("greaterThan", 
    function(value, element, params) {
        if (value == '') {
            return true;
        }
        if (!/Invalid|NaN/.test(new Date(value))) {
            return new Date(value) > new Date($(params).val());
        }
        return isNaN(value) && isNaN($(params).val()) 
            || (Number(value) > Number($(params).val())); 
    },'{{ __('validation.custom.end_date') }}');

    $("#formdata").validate({
        rules:{
            name: {
                required: true,
                maxlength: 30
            },
            start_date: {
                required:true,
            },
            end_date: {
                greaterThan: "#start_date",
            },
            recurrence_type: {
                required:true,
            }
        },
        messages:{
            name:{
                required:'{{ __('validation.required', ['attribute' => 'event name']) }}',
                maxlength:'{{ __('validation.max.string', ['attribute' => 'event name','max'=>'30']) }}',
            },
            start_date:{
                required:'{{ __('validation.required', ['attribute' => 'start date']) }}',
            },
            recurrence_type:{
                required:'{{ __('validation.required', ['attribute' => 'recurrence type']) }}',
            },
        },
        submitHandler: function (form) {
            var url = "{{ route('event.store') }}";
            $.ajax({
                type: 'POST',
                url: url,
                data: $(form).serialize(),
                dataType: "json",
                success: function(data) {
                    $("#formdata")[0].reset();
                    var myModalEl = $('#eventModalCenter');
                    var modal = bootstrap.Modal.getInstance(myModalEl)
                    modal.hide();
                    calendar.refetchEvents();
                },
                error: function() {
                    // alert('error handling here');
                }
            });
        }
    });
    $('#add_event').on('click',function(){
        $('#event_id').val('');
        $('#action').val('add');
        var myModal = document.getElementById('eventModalCenter');
        var modal = bootstrap.Modal.getOrCreateInstance(myModal)
        modal.show()
    });
    $('.close_modal').on('click',function(){
        var myModalEl = $('#eventModalCenter');
                    var modal = bootstrap.Modal.getInstance(myModalEl)
                    modal.hide();
    });
</script>
@endpush
@endsection
