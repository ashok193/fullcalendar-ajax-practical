@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ __('Event Detail') }}
                    
                </div>
                <div class="row p-5">
                    <div class="card">
                        <div class="card-body">
                          <h5 class="card-title">Event Name : {{ $event_detail->name }}</h5>
                          @if ($event_detail->description != '')
                          <p class="card-text">Event Description : {{ $event_detail->description }}</p>
                          @endif
                          <p class="card-text">Start Date : {{ $event_detail->start_date }}</p>
                          @if ($event_detail->end_date != '')
                          <p class="card-text">End Date : {{ $event_detail->end_date }}</p>
                          @endif
                        </div>
                    </div>
                </div>

                <div class="row px-4">
                    @foreach ($other_events as $event)
                    <div class="col-md-6 mt-5">
                        <div class="card" style="width: 18rem;">
                            <div class="card-body">
                            <h5 class="card-title">Event Date : {{ $event['title'] }}</h5>
                            @if ($event['description'] != '')
                            <p class="card-text">Event Description : {{ $event['description'] }}</p>
                            @endif
                            <p class="card-text">Event Date : {{ $event['start'] }}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
