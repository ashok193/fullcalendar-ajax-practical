import { Calendar } from 'fullcalendar'
import Swal from 'sweetalert2'

$.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
const BASE_URL = $('#base_url').val();
document.addEventListener('DOMContentLoaded', function() {
    const calendarEl = document.getElementById('calendar')
    const calendar = new Calendar(calendarEl, {
      initialView: 'timeGridDay',
      headerToolbar: {
        left: 'today prev,next',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay,multiMonthYear,listYear'
      },
      events: BASE_URL + "/event-list",
      // eventClick: function(info) {
      //   alert('Event: ' + info.event.title);
      //   alert('Coordinates: ' + info.jsEvent.pageX + ',' + info.jsEvent.pageY);
      //   alert('View: ' + info.view.type);
    
      //   // change the border color just for fun
      //   info.el.style.borderColor = 'red';
      // },
      eventDidMount: function(info) {
          $(info.el)
          .find(".fc-event-main")
          .after("<a href='javascript:;' style='color:red;font-size:17px;margin-left: 5px;'><span class='delete_event_button material-icons'><i class='fa fa-remove'></i></span></a>");
          $(info.el).find(".delete_event_button").on("click", function() {
              var id = info.event.id;
              Swal.fire({
                title: 'Are you sure you want to delete the event?',
                showDenyButton: false,
                showCancelButton: true,
                confirmButtonText: 'Delete',
              }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'POST',
                        url: BASE_URL+'/event-delete',
                        data: {id:id},
                        dataType: "json",
                        success: function(data) {
                            calendar.refetchEvents();
                        },
                        error: function() {
                            // alert('error handling here');
                        }
                    });
                }
              });
          }); 

          $(info.el)
          .find(".fc-event-main")
          .after("&nbsp;&nbsp;<a href='javascript:;' style='color:green;font-size:17px;margin-left: 5px;'><span class='edit_event_button material-icons'><i class='fa fa-edit'></i></span></a>");
          $(info.el).find(".edit_event_button").on("click", function() {
              var id = info.event.id;
              $.ajax({
                  type: 'GET',
                  url: BASE_URL+'/event-detail',
                  data: {id:id},
                  dataType: "json",
                  success: function(data) {
                      var event_data = data.result;
                      $('#name').val(event_data.name);
                      $('#description').val(event_data.description);
                      $('#start_date').val(event_data.start_date);
                      $('#end_date').val(event_data.end_date);
                      $('#recurrence_type').val(event_data.recurrence_type);
                      $('#event_id').val(event_data.id);
                      $('#action').val('edit');
                      var myModal = document.getElementById('eventModalCenter');
                      var modal = bootstrap.Modal.getOrCreateInstance(myModal)
                      modal.show();
                  },
                  error: function() {
                      // alert('error handling here');
                  }
              });
          }); 

          $(info.el)
          .find(".fc-event-main")
          .after("&nbsp;&nbsp;<a href='"+BASE_URL+"/event-view/"+info.event.id+"' target='_blank' style='color:#d9e0e9;font-size:17px;margin-left: 5px;'><span class='view_event_button material-icons'><i class='fa fa-eye'></i></span></a>");
      }
    })
    calendar.render()
    window.calendar = calendar;
});

