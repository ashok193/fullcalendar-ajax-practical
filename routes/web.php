<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/event-list', [EventController::class, 'index'])->name('event.list');
Route::post('/store', [EventController::class, 'store'])->name('event.store');
Route::post('/event-delete', [EventController::class, 'delete'])->name('event.delete');
Route::get('/event-detail', [EventController::class, 'detail'])->name('event.detail');
Route::get('/event-view/{id}', [EventController::class, 'view'])->name('event.view');
