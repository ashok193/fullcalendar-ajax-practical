<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'description',
        'start_date',
        'end_date',
        'recurrence_type',
    ];

    static function event_filter($start_date,$end_date,$type,$end_of_event=''){
        $condition = self::where('recurrence_type',$type);
        switch ($type) {
            case 'Single':
                $condition = $condition->where(function($query) use ($start_date,$end_date){
                    $query->whereDate('start_date','>=',$start_date)
                    ->whereDate('start_date','<=',$end_date);
                });
                break;
            case 'Daily':
                $condition =  $condition->whereDate('start_date','<=',$end_date)->where(function($query) use ($start_date,$end_date){
                    $query->whereDate('end_date','>=',$start_date)
                    ->orwhereNull('end_date');
                });
                break;
            case 'Monthly':
                $condition =  $condition->whereDate('start_date','<=',$end_date)->where(function($query) use ($start_date,$end_date){
                    $query->whereDate('end_date','>=',$start_date)
                    ->orwhereNull('end_date');
                });
                break;
            case 'Weekly':
                $condition =  $condition->whereDate('start_date','<=',$end_date)->where(function($query) use ($start_date,$end_date){
                    $query->whereDate('end_date','>=',$start_date)
                    ->orwhereNull('end_date');
                });
                break;
                
            case 'Yearly':
                $condition =  $condition->whereDate('start_date','<=',$end_date)->where(function($query) use ($start_date,$end_date){
                    $query->whereDate('end_date','>=',$start_date)
                    ->orwhereNull('end_date');
                });
                break;
            
            default:
                # code...
                break;
        }

        return $condition;
        
    }
    
}
