<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use Carbon\Carbon;

class EventController extends Controller
{
    public function index(Request $request){
        $start_date         = Carbon::parse($request->start)->format('Y-m-d');
        $end_date           = Carbon::parse($request->end)->format('Y-m-d');
        $event_list_single  = $this->get_single_event($start_date,$end_date);
        $event_list_daily   = $this->get_daily_event($start_date,$end_date);
        $event_list_monthly = $this->get_monthly_event($start_date,$end_date);
        $event_list_yearly  = $this->get_yearly_event($start_date,$end_date);
        $event_list_weekly  = $this->get_weekly_event($start_date,$end_date);

        $event_lists = array_merge(
            $event_list_daily,
            $event_list_single,
            $event_list_monthly,
            $event_list_yearly,
            $event_list_weekly
        );

        return response()->json($event_lists);
    }

    public function get_single_event($start_date,$end_date){
        $event_lists = Event::event_filter($start_date,$end_date,'Single')->get();
        $events = array();
        foreach ($event_lists as $key => $value) {
            $start = Carbon::parse($value->start_date)->format('Y-m-d');
            if (validateDate($start)) {
                $events[] = $this->get_event($start,$value);
            }
        }
        return $events;
    }
    public function get_daily_event($start_date,$end_date){
        $start_date = Carbon::parse($start_date)->format('Y-m-d');
        $end_date = Carbon::parse($end_date)->format('Y-m-d');

        $events = array();
        $event_lists = Event::event_filter($start_date,$end_date,'Daily')->get();
        foreach ($event_lists as $key => $value) {
            if ($start_date >= $value->start_date) {
                $start_date = $value->start_date;
            }

            if ($value->end_date != '' && $end_date >= $value->end_date) {
                $end_date = $value->end_date;
            }

            $month_list = getDayListFromDate($start_date,$end_date);
            foreach ($month_list as $key => $value_month) {
                $month = Carbon::parse($value->start_date)->format('Y-m-d');
                $start = $value_month;
                if ($month <= $value_month && validateDate($start)) {
                    $events[] = $this->get_event($start,$value);
                }
            }
        }
        return $events;
    }
    public function get_monthly_event($start_date,$end_date){
        $events = array();
        $event_lists = Event::event_filter($start_date,$end_date,'Monthly')->get();
        foreach ($event_lists as $key => $value) {
            $start_date = Carbon::parse($start_date)->format('Y-m-d');
            $end_date = Carbon::parse($end_date)->format('Y-m-d');

            if ($start_date >= $value->start_date) {
                $start_date = $value->start_date;
            }

            if ($value->end_date != '' && $end_date >= $value->end_date) {
                $end_date = $value->end_date;
            }

            $month_list = getMonthListFromDate($start_date,$end_date);
            // print_r($month_list);
            foreach ($month_list as $key => $value_month) {
                $month = Carbon::parse($value->start_date)->format('Y-m');
                if ($value->end_date != '') {
                    $end_month = Carbon::parse($value->end_date)->format('Y-m');
                }else{
                    $end_month = Carbon::parse($end_date)->format('Y-m');
                }
                $start = $value_month.'-'.Carbon::parse($value->start_date)->format('d');
                if ($month <= $value_month && $end_month >= $value_month && validateDate($start)) {
                    $events[] = $this->get_event($start,$value);
                }
            }
        }
        return $events;
    }

    public function get_weekly_event($start_date,$end_date){
        $events = array();
        $event_lists = Event::event_filter($start_date,$end_date,'Weekly')->get();
        foreach ($event_lists as $key => $value) {
            $start_date = Carbon::parse($start_date)->format('Y-m-d');
            $end_date = Carbon::parse($end_date)->format('Y-m-d');

            if ($start_date >= $value->start_date) {
                $start_date = $value->start_date;
            }

            if ($value->end_date != '' && $end_date >= $value->end_date) {
                $end_date = $value->end_date;
            }

            $week_list = getWeekListFromDate($start_date,$end_date,$value->start_date);
            foreach ($week_list as $key => $value_month) {
                $month     = Carbon::parse($value->start_date)->format('Y-m-d');
                $end_month = Carbon::parse($value->end_date)->format('Y-m-d');
                $start     = $value_month;
                if ($month <= $value_month  && validateDate($start)) {
                    $events[] = $this->get_event($start,$value);
                }
            }
        }
        return $events;
    }

    public function get_yearly_event($start_date,$end_date){
        $events = array();
        $event_lists = Event::event_filter($start_date,$end_date,'Yearly')->get();
        foreach ($event_lists as $key => $value) {
            $start_date = Carbon::parse($start_date)->format('Y-m-d');
            $end_date = Carbon::parse($end_date)->format('Y-m-d');

            if ($start_date >= $value->start_date) {
                $start_date = $value->start_date;
            }

            if ($value->end_date != '' && $end_date >= $value->end_date) {
                $end_date = $value->end_date;
            }

            $month_list = getYearListFromDate($start_date,$end_date);
            foreach ($month_list as $key => $value_month) {
                $month = Carbon::parse($value->start_date)->format('Y');
                $start = $value_month.'-'.Carbon::parse($value->start_date)->format('m-d');
                if ($month <= $value_month && validateDate($start)) {
                    $events[] = $this->get_event($start,$value);
                }
            }
        }
        return $events;
    }
    public function get_event($start,$value){
        $response_array = array();
        if ($value) {
            $response_array = [
                'id' => $value->id,
                'description' => $value->description,
                'title' => $value->name,
                'start' => $start,
                'allDay' => true
            ];
        }
        return $response_array;
    }
    public function store(Request $request){
        $event_data = [
            'name'            => $request->name,
            'description'     => $request->description,
            'start_date'      => $request->start_date,
            'end_date'        => $request->end_date,
            'recurrence_type' => $request->recurrence_type,
        ];
        if ($request->action == 'edit') {
            Event::whereId($request->event_id)->update($event_data);
        }else{
            Event::create($event_data);
        }
        return response()->json([
            'status' => 200
        ]);
    }
    public function delete(Request $request){
        Event::find($request->id)->delete();
        return response()->json([
            'status' => 200
        ]);
    }
    public function detail(Request $request){
        $event_detail = Event::find($request->id);
        return response()->json([
            'status' => 200,
            'result' => $event_detail
        ]);
    }
    public function view($id){
        $event_detail = Event::find($id);
        $events = array();
        if ($event_detail->recurrence_type == 'Daily') {
            $start_date = $event_detail->start_date;

            if ($event_detail->end_date == '') {
                $end_date = Carbon::parse($event_detail->start_date)->addDay(5)->format('Y-m-d');
            }
            $end_date = Carbon::parse($event_detail->start_date)->addDay(5)->format('Y-m-d');

            $month_list = getDayListFromDate($start_date,$end_date);
            foreach ($month_list as $key => $value_month) {
                $month = Carbon::parse($event_detail->start_date)->format('Y-m-d');
                $start = $value_month;
                if ($month <= $value_month && validateDate($start)) {
                    $events[] = $this->get_event($start,$event_detail);
                }
            }
        }
        $data['event_detail'] = $event_detail;
        $data['other_events'] = array_slice($events, 0, 5, true);
        return view('event_detail',$data);
    }
}
